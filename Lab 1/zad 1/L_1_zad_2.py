def read(txt):     # open the text file
    sf = open(txt, 'r').read()
    sf = ''.join([i for i in sf if i.isalpha()])
    sf = sf.lower()
    return sf


def overwrite(cipher):      # save ciphered text
    sf = open('cipher.txt', 'w+')
    sd = ''.join(map(str, cipher))
    sf.write(sd)


def alfa():     # create a dictionaries
    ky = 0
    alphabet = {}       # 1st with letters as values and digits as keys
    tebahpla = {}       # 2nd opposite
    ls = list('abcdefghijklmnopqrstuvwxyz')
    for i in ls:
        alphabet[i] = ky
        tebahpla[ky] = i
        ky += 1
    return alphabet, tebahpla


def cipher_f(key, text, tebahpla):    # cipher text
    cipher_int = []
    cipher_char = []
    for i in range(len(text)):
        cipher_int.append((text[i] + key[i % len(key)]) % 26)
    for j in cipher_int:
        cipher_char.append(tebahpla[j])
    return cipher_char


def decipher_f(key, text, tebahpla):    # cipher text
    cipher_int = []
    cipher_char = []
    for i in range(len(text)):
        cipher_int.append((text[i] - key[i % len(key)]) % 26)
    for j in cipher_int:
        cipher_char.append(tebahpla[j])
    return cipher_char


def coinc_index(txt):
    suma = 0
    a, b = alfa()
    text = txt
    for i in a:
        suma += text.count(i)*(text.count(i)-1)
        I = suma/(len(text)*(len(text)-1))
    return I


def coinc_column(cipher, column_int):
    x = list(cipher)
    col = {}
    suma = 0
    cols = []
    for i in range(column_int):
        col[i] = x[i::column_int]     # dictionary key: column number, value: elements of columns
        I = coinc_index(col[i])        # calculate coincidence index of all columns
        print('Column:', i+1, '\n', ''.join(col[i]), '\n', 'Coincidence index is equal:', I, "\n")
        cols.append(col[i])     # # list of list of elements of columns
        suma += I
    suma = suma/column_int
    if 0.065 < suma <= 0.069:
        print("Key length is", column_int, "\n")
    else:
        print("Key length is not", column_int, "\n")
    return col, column_int


def key_index(htebalfa, x_i, x_j):
    suma = 0
    for j in range(25):
        suma += x_i.count(htebalfa[j])*x_j.count(htebalfa[j])
    M_I = suma/(len(x_i)*len(x_j))
    return M_I


def mutal_coinc_index(k, cols, htebalfa):
    for j in range(k):
        print("Index of mutal coincidence between column", j, "and", (j+1) % k, "is", key_index(htebalfa, cols[j], cols[(j+1)%k]))


def find_key(cols, tebaphla, alphabet, cipp):
    klucz_dict = {}
    opp = list(cipp)
    cip = []
    for q in opp:
        cip.append(alphabet[q])
    for m in range(26):
        klucz_1 = tebaphla[m]
        y = list(klucz_1)
        key_1 = []
        for n in y:
            key_1.append(alphabet[n])

        for i in range(len(cols)):
            tekst_1 = cols[i]
            x = list(tekst_1)
            text_1 = []
            for j in x:
                text_1.append(alphabet[j])

            tekst_2 = cols[(i+1) % len(cols)]
            x = list(tekst_2)
            text_2 = []
            for k in x:
                text_2.append(alphabet[k])

            for l in range(26):
                klucz_2 = tebaphla[l]
                x = list(klucz_2)
                key_2 = []
                for v in x:
                    key_2.append(alphabet[v])
                st = cipher_f(key_1, text_1, tebaphla)
                nd = cipher_f(key_2, text_2, tebaphla)
                if 0.065 < key_index(tebaphla, st, nd) <= 0.068:
                    if i == 0 and (i+1)%len(cols) == 1:
                        klucz_key = '0 -> 1'
                        klucz_dict[klucz_key] = [key_1[0] - key_2[0]]
                    if i == 1 and (i+1)%len(cols) == 2:
                        klucz_key = '1 -> 2'
                        klucz_dict[klucz_key] = [key_1[0] - key_2[0]]
                    if i == 0 and (i+1)%len(cols) == 2:
                        klucz_key = '0 -> 2'
                        klucz_dict[klucz_key] = [key_1[0] - key_2[0]]
    #  abcdefghijklmnopqrstuvwxyz
    b = klucz_dict['0 -> 1'][0]
    c = klucz_dict['1 -> 2'][0]
    count_e = {}
    for a in range(26):
        klucz_char = []
        klucz = [a, (a + b) % 26, (a + b + c) % 26]
        decip = decipher_f(klucz, cip, tebaphla)
        for i in range(3):
            klucz_char.append(tebaphla[klucz[i]])
        count_e[''.join(klucz_char)] = decip.count('e')
    s = [(k, count_e[k]) for k in sorted(count_e, key=count_e.get, reverse=True)]
    key_final = []
    k_f_list = list(s[0][0])
    for v in k_f_list:
        key_final.append(alphabet[v])

    print("\n", "Key:", s[0][0],"   Deciphered text: ", ''.join(decipher_f(key_final, cip, tebaphla)))


columns = 3  #int(input("how many columns: "))
alphabet, tebaphla = alfa()

klucz = input("input key: ")
y = list(klucz)
key = []
for i in y:
    key.append(alphabet[i])

text = read('tekst.txt')
x = list(text)
txt = []
for i in x:
    txt.append(alphabet[i])

cipher = cipher_f(key, txt, tebaphla)
overwrite(cipher)
cip = read('cipher.txt')
print(text)
print("Coincidence index of the text", coinc_index(txt),"\n")
cols, key = coinc_column(cip, columns)
mutal_coinc_index(key, cols, tebaphla)
find_key(cols, tebaphla, alphabet, cip)
