__author__ = 'Piotr Babicz'


import math


def pow_mod(base, exp, mod):
    pows = []
    t_exp = exp

    while t_exp > 1:    # sumy jak najwiekszych poteg dwojki, tak aby suma byla rowna exp
        pows.append(pow_2(t_exp))
        t_exp = t_exp - pow_2(t_exp)
    if t_exp == 1:
        pows.append(1)

    temp = 1
    t_base = []
    curr = base % mod
    t_base.append(curr)

    while temp < exp:    # kalkulacja kolejnych poteg base
        curr = (pow(curr, 2) % mod)
        t_base.append(curr)
        temp *= 2

    fin = 1
    for i in pows:
        fin *= t_base[(int(math.log(i, 2)))]
    return fin % mod


def pow_2(n):   # returns highest power of 2 that is equal or smaller than n
    temp = 1
    for i in range(int(pow(2, 31))):
        curr = 1 << i   # left bitwise shift
        if curr > n:
            break
        temp = curr
    return temp


def pier(a):
    even = True
    cout = 0
    '''1'''
    while even:
        if int(a / 2) * 2 == a:
            cout += 1
            a = int(a / 2)
        else:
            even = False
    '''2'''
    x = int(math.sqrt(a))
    if x != math.sqrt(a):
        x = x + 1
    elif x == math.sqrt(a):
        return x, x
    '''3'''
    while x < (a + 1) / 2:
        y_2 = pow(x, 2) - a
        if y_2 > 0 and int(math.sqrt(y_2)) == math.sqrt(y_2):
            d_1 = int(x + math.sqrt(y_2))
            d_2 = int(x - math.sqrt(y_2))
            return d_1, d_2
        x += 1


def drzewo(une, deux):
    lista = []

    def rek(une, deux, lista):
        while une is not None:
            try:
                une, deux = pier(une)
                lista.append(une)
                lista.append(deux)
                rek(deux, une, lista)
            except TypeError:
                break
    rek(une, deux, lista)
    return lista


def fermat(a):  # algorytm fermata
    dict = {}
    try:
        d_1, d_2 = pier(a)
    except TypeError:
        even = True
        cout = 0
        '''1'''
        while even:
            if int(a / 2) * 2 == a:
                cout += 1
                a = int(a / 2)
            else:
                even = False
        if a != 1:
            dict[a] = 1
        if cout > 0:
            dict[2] = cout
        return dict
    lista_0 = [2] * int(math.log(a / (d_1 * d_2), 2))
    if pier(d_1) is None:
        lista_0.append(d_1)
    if pier(d_2) is None:
        lista_0.append(d_2)
    lista_1 = drzewo(d_1, d_2)
    lista_2 = drzewo(d_2, d_1)
    ogromna_lista = lista_0 + lista_1 + lista_2
    temp = []
    for u in ogromna_lista:
        for num in range(len(ogromna_lista)):
            if u * ogromna_lista[num] in ogromna_lista:
                temp.append(u * ogromna_lista[num])
    for y in temp:
        try:
            ogromna_lista.remove(y)
        except ValueError:
            pass
    for i in ogromna_lista:
        dict[i] = ogromna_lista.count(i)
    return dict


def lucas(num, mod):
    dict = fermat(num - 1)
    print('{:19}'.format("%i ^ %i %s") % (mod, int(num - 1), " "),
          "= %i (mod %i)" % (pow_mod(mod, int(num - 1), num), num))
    for key in sorted(dict):
        print('{:19}'.format("%i ^ (%i / %i)") % (mod, int(num - 1), key),
              "= %i (mod %i)" % (pow_mod(mod, int((num - 1) / key), num), num))
        if pow_mod(mod, int((num - 1) / key), num) == 1:
            print("test nie rozstrzyga czy liczba %i jest pierwsza" % num)
            return None
    print("liczba %i jest pierwsza" % num)


if __name__ == "__main__":
    choose = input("choose potegowanie modulo, Fermat, Lucas (p, f, l): ")
    if choose == 'p' or choose == "P":
        base = int(input("base:"))
        exp = int(input("exp:"))
        mod = int(input("mod:"))
        print("%i ^ %i = %i (mod %i)" % (base, exp, pow_mod(base, exp, mod), mod))
    elif choose == 'f' or choose == "F":
        liczba_do_fermata = int(input("do Fermata: "))
        dict = fermat(liczba_do_fermata)
        print("Liczba:", liczba_do_fermata, "\n"'{:<9}'.format("Dzielniki"), '{:>10}'.format("Krotnosc"))
        for key in sorted(dict):
            print('{:<9}'.format(key), "|", dict[key])
    elif choose == 'l' or choose == "L":
        n = int(input("n: "))
        q = int(input("q: "))
        lucas(n, q)
