#   class for operations on strings (ex. x^3 * x^2)
class PowOpe:
    def __init__(self, a):
        self.a = a

    def __mul__(self, other):
        return "x^" + str(stoi(self.a) + stoi(other.a))

    def __truediv__(self, other):
        return "x^" + str(stoi(self.a) - stoi(other.a))


def pad(str):
    return '{:12}'.format(str)


def load_txt(name_1, name_2, bits):
    txt_3 = []
    txt_4 = []
    hex_to_bin = {'0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100', '5': '0101',
                  '6': '0110', '7': '0111', '8': '1000', '9': '1001', 'a': '1010', 'b': '1011',
                  'c': '1100', 'd': '1101', 'e': '1110', 'f': '1111'}

    key = input(name_1 + " (in hexadecimal notation, without 0x): ")
    txt = input(name_2 + " (in hexadecimal notation, without 0x): ")
    if bits == 4:
        return list(txt), key
    print(pad(name_1), '0x' + key)
    print(pad(name_2), '0x' + txt)
    for j in list(txt):
        txt_3.append(hex_to_bin[j])
    x = ''.join(txt_3)
    if len(x) % 16 != 0 and bits == 16:
        txt_3.append('0' * (16 - len(x)))

    for i in list(key):
        txt_4.append(hex_to_bin[i])
    y = ''.join(txt_4)
    if len(y) % 16 != 0 and bits == 16:
        txt_4.append('0' * (16 - len(y)))
    return list(''.join(txt_3)), ''.join(txt_4)


# converts str (ex. x^3) to int 3 and add them
def stoi(string):
    return int(''.join(c for c in string if c.isdigit() or c == '-'))


#   function to binary add two list of bin nums
def bin_add(list_1, list_2):
    lis = ['0']*len(list_1)
    for i in range(len(list_1)):
        if list_1[i] == list_2[i]:
            if list_1[i] == 0 or list_1[i] == '0':
                lis[i] = '0'
            elif list_1[i] == 1 or list_1[i] == '1':
                lis[i] = '0'
        elif list_1[i] != list_2[i]:
            lis[i] = '1'
    return lis


def ZK(lis):
    new = lis
    temp_1 = lis[8:12]
    temp_2 = lis[12:16]
    del new[8:16]
    new = new + temp_2 + temp_1
    return new


def SboxE(li):
    box = {'0000': '1110', '0001': '0100', '0010': '1101', '0011': '0001', '0100': '0010', '0101': '1111',
           '0110': '1011', '0111': '1000', '1000': '0011', '1001': '1010', '1010': '0110', '1011': '1100',
           '1100': '0101', '1101': '1001', '1110': '0000', '1111': '0111', }
    return box[li]


def SboxD(li):
    box = {'0000': '1110', '0001': '0011', '0010': '0100', '0011': '1000', '0100': '0001', '0101': '1100',
           '0110': '1010', '0111': '1111', '1000': '0111', '1001': '1101', '1010': '1001', '1011': '0110',
           '1100': '1011', '1101': '0010', '1110': '0000', '1111': '0101', }
    return box[li]


def FS_box(char, lis):
    new = []
    if char == 'D':
        for i in range(4):
            new.append(SboxD(lis[0:4]))
            lis = list(lis)
            del lis[0:4]
            lis = ''.join(lis)
    else:
        for i in range(4):
            new.append(SboxE(lis[0:4]))
            lis = list(lis)
            del lis[0:4]
            lis = ''.join(lis)
    return new


def key_gen(key):
    k_1 = []
    k_2 = []

    k_1_00 = bin_add(bin_add(list(key[0:4]), list(SboxE(key[12:16]))), list('0001'))
    k_1_10 = bin_add(list(key[8:12]), k_1_00)
    k_1_01 = bin_add(list(key[4:8]), k_1_10)
    k_1_11 = bin_add(list(key[12:16]), k_1_01)

    k_1.append(k_1_00)
    k_1.append(k_1_01)
    k_1.append(k_1_10)
    k_1.append(k_1_11)

    k_2_00 = bin_add(bin_add(k_1_00, list(SboxE(''.join(k_1_11)))), list('0010'))
    k_2_10 = bin_add(k_1_10, k_2_00)
    k_2_01 = bin_add(k_1_01, k_2_10)
    k_2_11 = bin_add(k_1_11, k_2_01)

    k_2.append(k_2_00)
    k_2.append(k_2_01)
    k_2.append(k_2_10)
    k_2.append(k_2_11)
    new_1 = []
    for a in k_1:
        for b in a:
            new_1.append(b)
    new_2 = []
    for c in k_2:
        for d in c:
            new_2.append(d)

    return ''.join(new_1), ''.join(new_2)


#   operation ⊕ in Mini-AES cipher
def mul(l_1, l_2):
    x_l = ['x^3', 'x^2', 'x^1', 'x^0']
    red = ['x^4', 'x^1', 'x^0']
    pol = {'x^3': [1, 0, 0, 0],
           'x^2': [0, 1, 0, 0],
           'x^1': [0, 0, 1, 0],
           'x^0': [0, 0, 0, 1]}

    if l_1 == ['0', '0', '0', '0']:
        return [0, 0, 0, 0]

    elif l_2 == ['0', '0', '0', '0']:
        return [0, 0, 0, 0]

    lx_1 = []
    lx_2 = []

    for i in range(4):
        lx_1.append(int(l_1[i]) * x_l[i])
        lx_2.append(int(l_2[i]) * x_l[i])   # converts bin list to polynomial

    c_list = []
    for i in range(len(l_1)):
        temp_1 = PowOpe(lx_1[i])
        for j in range(len(l_2)):
            temp_2 = PowOpe(lx_2[j])
            try:
                c_list.append(temp_1 * temp_2)
            except ValueError:
                pass

    product = []
    for j in range(7, -1, -1):
        if c_list.count('x^%i' % j) % 2 != 0:
            product.append('x^%i' % j)
    if not product:
        product = lx_1
        product = [value for value in product if value != '']
        print("pr", product)
    temp_3 = PowOpe(product[0])     # loop handling long multiplication
    temp_4 = PowOpe(red[0])
    new_line = []
    while stoi(temp_3 / temp_4) >= 0:
        sub_line = []
        new_line = []
        temp_5 = PowOpe(temp_3 / temp_4)
        for a in red:
            sub_line.append(temp_5 * PowOpe(a))
        for b in sub_line:
            if b not in product:
                new_line.append(b)
        for c in product:
            if c not in sub_line:
                new_line.append(c)
        new_line.sort(reverse=True)
        product = new_line
        temp_3 = PowOpe(new_line[0])

    final = [0] * 4
    for d in range(len(new_line)):
        final = [x + y for x, y in zip(final, pol[new_line[d]])]
    if final == [0, 0, 0, 0]:
        for d in range(len(product)):
            final = [x + y for x, y in zip(final, pol[product[d]])]
        return final
    else:
        return final


def multiplication(l_1, l_2):
    new = []
    new.append(bin_add(mul(l_1[0:4], l_2[0:4]), mul(l_1[4:8], l_2[8:12])))
    new.append(bin_add(mul(l_1[0:4], l_2[4:8]), mul(l_1[4:8], l_2[12:16])))
    new.append(bin_add(mul(l_1[8:12], l_2[0:4]), mul(l_1[12:16], l_2[8:12])))
    new.append(bin_add(mul(l_1[8:12], l_2[4:8]), mul(l_1[12:16], l_2[12:16])))

    new_1 = []
    for a in new:
        for b in a:
            new_1.append(b)
    return new_1


def crypt(t, k_p):
    m = list('0011001000100011')
    k_1, k_2 = key_gen(k_p)
    print(pad("key 1"), hex(int(k_1, 2)))
    print(pad("key 2"), hex(int(k_2, 2)))
    t = bin_add(t, k_p)      # Dodawanie klucza początkowego kp do tekstu
    t = FS_box('E', ''.join(t))     # Zastosowanie funkcji FSBox(E,t)
    t = ZK(list(''.join(t)))    # Zastosowanie funkcji ZK(t)
    t = multiplication(m, t)   # Przemnożenie tekstu t przez ciąg bitów m = (0,0,1,1,0,0,1,0,0,0,1,0,0,0,1,1): t = m·t
    t = bin_add(t, k_1)     # Dodawanie klucza rundy pierwszej k1 do tekstu t: t = t + k1.
    t = FS_box('E', ''.join(t))      # Zastosowanie funkcji FSBox(E,t)
    t = ZK(list(''.join(t)))        # Zastosowanie funkcji ZK(t)
    t = bin_add(t, k_2)         # Dodawanie klucza rundy drugiej k2 do tekstu t: t = t + k2.
    print(pad("szyfrogram:"), hex(int(''.join(t), 2)))


def main():
    choose = input("choose cipher or multiplication ⊕ (c, m):")
    if choose == 'c' or choose == "C":
        txt, klucz = load_txt("Key", "Text", 16)
        crypt(txt, klucz)
    elif choose == 'm' or choose == 'M':
        a, b = load_txt("a", "b", 4)
        wynik = mul(a, list(b))
        temp = ''.join(map(str,wynik))
        se = SboxE(temp)
        sd = SboxD(temp)
        print(pad("a⊕b ="), hex(int(''.join(map(str, wynik)), 2)))
        print(pad("SboxE"), hex(int(se, 2)))
        print(pad("SboxD"), hex(int(sd, 2)))

    else:
        print("Wrong char")
        main()


if __name__ == "__main__":
    main()

