PW = [1, 5, 2, 0, 3, 7, 4, 6]
cross = [4, 5, 6, 7, 0, 1, 2, 3]
PO = [3, 0, 2, 4, 6, 1, 7, 5]
P10 = [2, 4, 1, 6, 3, 9, 0, 8, 7, 5]
sl_1 = [1, 2, 3, 4, 0]
P10w8 = [5, 2, 6, 3, 7, 4, 9, 8]
sl_2 = [2, 3, 4, 0, 1]
P4w8 = [3, 0, 1, 2, 1, 2, 3, 0]
P4 = [1, 3, 2, 0]


def load_txt():
    txt_3 =[]
    hex_to_bin = {'0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100', '5': '0101',
                  '6': '0110', '7': '0111', '8': '1000', '9': '1001', 'a': '1010', 'b': '1011',
                  'c': '1100', 'd': '1101', 'e': '1110', 'f': '1111'}
    txt = input("Text to crypt (in hexadecimal notation, without 0x): ")
    print("text to cipher   ", '0x' + txt)
    for j in list(txt):
        txt_3.append(hex_to_bin[j])
    x = ''.join(txt_3)
    if len(x) % 8 != 0:
        txt_3.append('0' * 4)
    return list(''.join(txt_3))


def per(perm, list):
    pw = []
    for i in perm:
        pw.append(list[i])
    return pw


def bin_add(list_1, list_2):
    list = ['0']*len(list_1)
    for i in range(len(list_1)):
        if list_1[i] == list_2[i] == 0:
            list[i] = '1'
        elif list_1[i] == list_2[i] == 1:
            list[i] = '0'
        elif list_1[i] != list_2[i]:
            list[i] = '1'
    return list


def s_box_1(lista):
    res = []
    box = [['01', '00', '11', '10'],
           ['11', '10', '01', '00'],
           ['00', '10', '01', '11'],
           ['11', '01', '11', '10']]
    result = box[int('%i%i' % (int(lista[0]), int(lista[3])), 2)][int('%i%i' % (int(lista[1]), int(lista[2])), 2)]
    for i in list(result):
        res.append('%s' % (i))
    return res


def s_box_2(lista):
    res = []
    box = [['00', '01', '10', '11'],
           ['10', '00', '01', '11'],
           ['11', '00', '01', '00'],
           ['10', '01', '00', '11']]
    result = box[int('%i%i' % (int(lista[0]), int(lista[3])), 2)][int('%i%i' % (int(lista[1]), int(lista[2])), 2)]
    for i in list(result):
        res.append('%s' % (i))
    return res


key_prime = ['1', '1', '0', '0', '0', '0', '0', '0', '1', '1']
P10_k_p = per(P10, key_prime)
k_0_0 = P10_k_p[0:int(len(P10_k_p)/2)]
k_1_0 = P10_k_p[int(len(P10_k_p)/2):len(P10_k_p)]
k1 = per(P10w8, per(sl_1, k_0_0) + per(sl_1, k_1_0))
k2 = per(P10w8, (per(sl_2, per(sl_1, k_0_0)) + per(sl_2, per(sl_1, k_1_0))))
print("1st key:", ''.join(k1),
      "\n2nd key:", ''.join(k2))


txt = load_txt()
le = len(txt)
final = ''
print(''.join(txt))
# szyfrowanie
for i in range(int(le/8)):
    txt_0 = per(PW, txt[0:8])  # permutacja wstepna
    del txt[0:8]
    t_1 = txt_0[0:int(len(txt_0)/2)]
    t_2 = txt_0[int(len(txt_0)/2):len(txt_0)]

    suma = bin_add(per(P4w8,t_2), k1)   # szyfrowanie kluczem rundy 1
    s_b_1 = suma[0:int(len(suma)/2)]
    s_b_2 = suma[int(len(suma)/2):len(suma)]
    jeden = bin_add(per(P4, s_box_1(s_b_1) + s_box_2(s_b_2)), t_1)
    je = jeden + t_2
    print("SBox1 round 1:   ", ''.join(s_box_1(s_b_1)))
    print("SBox2 round 1:   ", ''.join(s_box_2(s_b_2)))
    print("after 1st round: ", hex(int(''.join(je), 2)))

    txt_2 = per(cross, je)       # krzyzowanie
    t_1_2 = txt_2[0:int(len(txt_2)/2)]
    t_2_2 = txt_2[int(len(txt_2)/2):len(txt_2)]

    suma_2 = bin_add(per(P4w8, t_2_2), k2)   # szyfrowanie kluczem rundy 2
    s_b_1_2 = suma_2[0:int(len(suma_2)/2)]
    s_b_2_2 = suma_2[int(len(suma_2)/2):len(suma_2)]
    dwa = bin_add(per(P4, s_box_1(s_b_1_2) + s_box_2(s_b_2_2)), t_1_2)
    dw = dwa + t_2_2
    print("SBox1 round 2:   ", ''.join(s_box_1(s_b_1_2)))
    print("SBox2 round 2:   ", ''.join(s_box_2(s_b_2_2)))
    print("after 2nd round: ", hex(int(''.join(dw), 2)))

    final += ''.join(per(PO, dw))

print("cryptogram:      ", hex(int(final, 2)))

# odszyfrowywywanie
fin = ''
final = list(final)
for i in range(int(le/8)):
    final_0 = per(PW, final[0:8])  # permutacja wstepna
    del final[0:8]
    t_1 = final_0[0:int(len(final_0) / 2)]
    t_2 = final_0[int(len(final_0) / 2):len(final_0)]

    suma_2 = bin_add(per(P4w8, t_2), k2)  # szyfrowanie kluczem rundy 2
    s_b_1_2 = suma_2[0:int(len(suma_2) / 2)]
    s_b_2_2 = suma_2[int(len(suma_2) / 2):len(suma_2)]
    dwa = bin_add(per(P4, s_box_1(s_b_1_2) + s_box_2(s_b_2_2)), t_1)
    dw = dwa + t_2

    final_2 = per(cross, dw)  # krzyzowanie
    t_1_2 = final_2[0:int(len(final_2) / 2)]
    t_2_2 = final_2[int(len(final_2) / 2):len(final_2)]

    suma = bin_add(per(P4w8, t_2_2), k1)  # szyfrowanie kluczem rundy 1
    s_b_1 = suma[0:int(len(suma) / 2)]
    s_b_2 = suma[int(len(suma) / 2):len(suma)]
    jeden = bin_add(per(P4, s_box_1(s_b_1) + s_box_2(s_b_2)), t_1_2)
    je = jeden + t_2_2

    fin += ''.join(per(PO, je))

print("odcryptogram:    ", hex(int(fin, 2)))
